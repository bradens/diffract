import sys
import os
import numpy as np
from diffract import single_DMD_scenario
from multiprocessing import Process


def run_single_DMD_scenario(spls, width, fill, tiles, wave, theta_x, theta_y, inc, DMD_ang, ID):
    """ Runs a single DMD scenario over a subset of theta_x """
    u_2 = single_DMD_scenario(spls, width, fill, tiles, wave, theta_x, theta_y, inc, DMD_ang)
    np.save('temp/{:03d}_output.npy'.format(ID), u_2)


def main(inputs):
    """ Segments the input theta_x array equally into n arrays for n
    processes. Runs each process in parallel. """
    inputs = inputs[1::]
    # Gather inputs values
    num_procs = int(inputs[0])
    spls = (int(inputs[1]), int(inputs[2]))
    width = (float(inputs[3]), float(inputs[4]))
    fill = float(inputs[5])
    wave = float(inputs[6])
    theta_x_start = float(inputs[7])
    theta_x_end = float(inputs[8])
    theta_x_del = float(inputs[9])
    theta_y_start = float(inputs[10])
    theta_y_end = float(inputs[11])
    theta_y_del = float(inputs[12])
    inc = (float(inputs[13]), float(inputs[14]))
    DMD_ang = (float(inputs[15]), float(inputs[16]))
    tiles = (int(inputs[17]), int(inputs[18]))

    # Reconstruct theta_x and theta_y
    theta_x = np.arange(theta_x_start, theta_x_end + theta_x_del / 2., theta_x_del)
    theta_y = np.arange(theta_y_start, theta_y_end + theta_y_del / 2., theta_y_del)

    # Determine number of samples for each process
    num_calcs = int(np.ceil(theta_x.size / num_procs))

    # Set up processes
    procs = []
    for proci in range(num_procs):
        theta_x_proc = theta_x[proci * num_calcs:(proci + 1) * num_calcs]
        if proci == (num_procs - 1):
            theta_x_proc = theta_x[proci * num_calcs::]
        proc = Process(target=run_single_DMD_scenario, args=(spls, width, fill, tiles, wave, theta_x_proc, theta_y, inc, DMD_ang, proci))
        procs.append(proc)
        proc.start()

    # Wait for processes to end
    for proc in procs:
        proc.join()

    # Join outputs into one variable
    output = np.zeros((len(theta_y), len(theta_x))) * 1j
    for out_i in range(len(procs)):
        if out_i == (num_procs - 1):
            output[:, out_i * num_calcs::] = np.load('temp/{:03d}_output.npy'.format(out_i))
        else:
            output[:, out_i * num_calcs:(out_i + 1) * num_calcs] = np.load('temp/{:03d}_output.npy'.format(out_i))
    np.save('temp/output.npy', output)


if __name__ == '__main__':
    if not os.path.exists('temp'):
        os.makedirs('temp')
    inputs = sys.argv
    main(inputs)
