import numpy as np
from numpy.fft import fft2, ifft2, fftshift
from itertools import product
from scipy.signal import convolve2d
import warnings
from subprocess import call

import matplotlib.pyplot as plt


def single_DMD_scenario_parallel(num_procs, spls, width, fill, tiles, wave, theta_x, theta_y, inc, DMD_ang):
    """Runs a single DMD scenario by spreading over multiple processors
    INPUTS
    ------
    num_procs : int
        Number of processors to use
    spls : tuple, float
        Number of (x, y) samples for each DMD mirror
    width : tuple, float
        Dimensions in (x, y) of each DMD mirror, [meters]
    fill : float
        Fill factor of DMD mirror
    tiles : tuple, float
        Number of tiles in (x, y) of scenario
    wave : float
        Wavelength of light, [meters]
    theta_x : array/list
        Output angles(s) in x direction to measure output over,[radians]
    theta_y : array/list
        Input angle(s) in y direction to measure output over, [radians]
    inc : tuple, float
        Incident angle of input light in (x, y), [radians]
    DMD_ang : tuple, float
        Angle of DMD mirrors in (x, y), [radians]
    RETURNS
    -------
    u_2 : array
        Array of dimensions (theta_y, theta_x) of the output far field
        diffracted light intensity: [E-field]^2
    """
    # Decopose theta_x into start, end, and delta
    if len(theta_x) == 1:  # Sigle theta_x
        theta_x_start = theta_x[0]
        theta_y_end = theta_x[0] + 0.5
        theta_y_del = 1.
    else:  # Multiple theta_x values
        theta_x_start = theta_x[0]
        theta_x_end = theta_x[-1]
        theta_x_del = np.diff(theta_x).mean()
    # Decompose theta_y into start, end, and delta
    if len(theta_y) == 1:  # Single theta_y value
        theta_y_start = theta_y[0]
        theta_y_end = theta_y[0] + 0.5
        theta_y_del = 1.
    else:  # Multiple theta_y vales
        theta_y_start = theta_y[0]
        theta_y_end = theta_y[-1]
        theta_y_del = np.diff(theta_y).mean()
    # Create input argument string
    input_args = list(map(str, [num_procs, spls[0], spls[1], width[0], 
                                width[1], fill, wave, theta_x_start,
                                theta_x_end, theta_x_del,theta_y_start,
                                theta_y_end, theta_y_del, inc[0], inc[1],
                                DMD_ang[0], DMD_ang[1], tiles[0], tiles[1]]))
    input_str = ''
    for i in range(len(input_args)):
        input_str += input_args[i] + ' '
    # Call parallel_DMD_scenario
    call('python parallel_DMD_scenario.py ' + input_str)
    return np.load('temp/output.npy')


def single_DMD_scenario(spls, width, fill, tiles, wave, theta_x, theta_y, inc, DMD_ang):
    """Performs a DMD scerario, with one input angle and one DMD angle
    configuration.  Returns far field diffraction pattern
    INPUTS:
        spls : tuple, int
            (x, y), number of sample points of each DMD mirror. Must be even in x and y.
        width : tuple, float
            (x, y), width in x and y or DMD mirror, [meters]
        fill : float
            Fill factor of DMD mirror
        tiles : tuple, int
            (x, y), number of DMD mirrors to tile together
        wave : float
            Wavelength of light [meters]
        theta_x : array, float
            Output angles to measure over, [radians]
        theta_y : array, float
            Output angles to measure over, [radians]
        inc : tuple, float
            (x, y), incident angle of light in x and y, [radians]
        DMD_ang : tuple, float
            (x, y), angle of DMD mirrors, [radians]
    RETURNS:
        Far field intensity pattern over defined output angles [E-field^2]
            """
    # TODO: Add warning if angular sampling is not fine enough to see output (fwhm of sinc)
    # Check that spls is even in both x and y
    if ((spls[0] % 2) == 1) or ((spls[1] % 2) == 1):
        raise ValueError('Samples must be even in both x and y')
    # Create mask (non-complex)
    mask = _create_DMD_mirror_mask(spls[0], spls[1], fill)
    # Store output in _sum
    _sum = np.zeros((len(theta_y), len(theta_x))) * 1j
    # Loop over given output angles
    for (m, x_ang), (n, y_ang) in product(enumerate(theta_x), enumerate(theta_y)):
        # Create base phase for this output angle
        d_phase_x = width[0] * (np.sin(inc[0] - DMD_ang[0]) - np.sin(DMD_ang[0] - x_ang))
        d_phase_y = width[1] * (np.sin(inc[1] - DMD_ang[1]) - np.sin(DMD_ang[1] - y_ang))
        phase, valid = _add_linear_phase(mask, wave, d_phase_x, d_phase_y)
        if valid == 0:
            _sum[n, m] = 0
            continue
        # Tile mirrors
        phase_tiled = _tile_DMD_phase(tiles[0], tiles[1], phase, width[0], width[1], inc[0], inc[1], x_ang, y_ang, wave)
        # Perform summation for current output angle
        # Apply cosine falloff factor for each output angle here
        _sum[n, m] = _perform_summation(phase_tiled) * np.cos(DMD_ang[0] - x_ang) * np.cos(DMD_ang[1] - y_ang)
    # Apply the input cosine falloff factor here and normalize itensity
    norm_fac = np.sum(mask) * (((tiles[0] * tiles[1])) + ((tiles[0] - 1) * (tiles[1] - 1)))
    return np.abs(_sum ** 2) * np.cos(inc[0] - DMD_ang[0]) * np.cos(inc[1] - DMD_ang[1]) / (norm_fac ** 2)  # Intensity


def _tile_DMD_phase(n_x, n_y, DMD_mirror, x_width, y_width, x_inc, y_inc, x_ang, y_ang, wave):
    """ Tiles DMD mirrors in x and y and adds appropriate constant phase to
    each mirror
    PARAMETERS:
    ----------
    n_x : number of x tiles
    n_y : number of y tiles
    DMD_mirror : single DMD mirror with correct phase
    x_width : pitch of one mirror in x, meters
    y_width : pitch of one mirror in y, meters
    x_inc : x incident angle of light, rad
    y_inc : y incident angle of light, rad
    x_ang : observation angle in x, rad
    y_ang : observation angle in y, rad
    wave : wavelength, meters
    RETURNS
    -------
    Tiled complex array of DMD mirrors"""
    if n_x == 0 or n_y == 0:
        raise ValueError('Number of tiles cannot be 0 in x or y.')
    y_dim, x_dim = DMD_mirror.shape  # Pixel dimensions of one DMD mirror
    DMD_template = np.zeros((n_y * y_dim, n_x * x_dim)) * 1j  # Template of output phase map
    # Add "outer" mirrors
    for x_i in np.arange(0, n_x):
        for y_i in np.arange(0, n_y):
            cur_dist = (n_x - x_i) * x_width * np.sin(x_inc) + x_i *x_width * np.sin(-x_ang) + \
                (n_y - y_i) * y_width * np.sin(y_inc) + y_i * y_width * np.sin(-y_ang)
            DMD_template[y_i * y_dim:(y_i + 1) * y_dim, x_i * x_dim:(x_i + 1) * x_dim] += \
                DMD_mirror * np.exp(1j * 2 * np.pi / wave * cur_dist)
    # Add "inner" mirrors
    for x_i in np.arange(1, n_x):
        for y_i in np.arange(1, n_y):
            cur_dist = (n_x - x_i + 0.5) * x_width * np.sin(x_inc) + (x_i - 0.5) * x_width * np.sin(-x_ang) + \
                (n_y - y_i + 0.5) * y_width * np.sin(y_inc) + (y_i - 0.5) * y_width * np.sin(-y_ang)
            DMD_template[int(y_i * y_dim - y_dim / 2):int((y_i + 1) * y_dim - y_dim / 2),\
                int(x_i * x_dim - x_dim / 2):int((x_i + 1) * x_dim - x_dim / 2)] += \
                DMD_mirror * np.exp(1j * 2 * np.pi / wave * cur_dist)
    return DMD_template


def _create_DMD_mirror_mask(x_spls, y_spls, fill=1.):
    """ Creates one DMD mirror with tilt phase
    x_spls : int
        Number of x samples across the mirror phase array
    y_spls : int
        Number of y samples across the mirror phase array
    fill : float, 0 to 1
        Fill factor (If looking allong diagonal slice of DMD)
    """
    # TODO: Make fill factor an area fill factor
    if fill > 1 or fill <= 0:
        raise ValueError('Fill must be between 0 and 1.')
    x = np.linspace(-0.5, 0.5, x_spls + 1)[0:x_spls]
    y = np.linspace(-0.5, 0.5, y_spls + 1)[0:y_spls]
    X, Y = np.meshgrid(x, y)
    full_fill = (x.max() - x.min() + y.max() - y.min()) / 4.
    mask = (np.abs(X + Y) < (fill * full_fill)) * (np.abs(X - Y) < (fill * full_fill)).astype(np.int)
    return mask


def far_field(mask, width, wave, theta_x, theta_y, inc=(0, 0), tile=(1, 1)):
    """ Computes the far field diffraction patten of an arbitrary aperture
    defined by mask. X and Y tilt can both be included to the wavefront
    incident on the aperture.
    INPUT
    -----
    mask : 2D array, float
        Mask of u_1 data
    width : tuple, float
        Width of mask (x_width, y_width), from edge to edge, [meters]
    wave : float
        wavelength of light used, [meters]
    theta_x : 1D array, float
        Angles in x direction that output will be sampled at, [radians]
    theta_y : 1D array, float
        Angles in y direction that output will be sampled at, [radians]
    inc : tuple, float, default=(0, 0)
        Incident angles (x_inc, y_inc) of incoming wavefront, [radians]
    tile : tuple, integer, default=(1, 1)
        Number of tiles in (x, y) of input mask
    OUTPUT
    ------
    u_2 : array, float
        Far field diffraction pattern intensity
    NOTES
    -----
        This function assumes transmission, not reflection.  Thus, the
        incident angles are multiplied by -1 before the phase is added.  If
        reflection is desired, manually multiply the incidence angles by -1.
    """
    # Tile input mask
    mask = _tile_mask(tile[0], tile[1], mask)
    x_width = width[0] * tile[0]
    y_width = width[1] * tile[1]
    # Store output in _sum
    _sum = np.zeros((len(theta_y), len(theta_x))) * 1j
    warnings.resetwarnings()
    for (m, x_ang), (n, y_ang) in product(enumerate(theta_x), enumerate(theta_y)):
        # Define wavefront at aperture
        d_phase_x = x_width * (np.sin(-inc[0]) + np.sin(x_ang))
        d_phase_y = y_width * (np.sin(-inc[1]) + np.sin(y_ang))
        # Add phase to mask data
        u_1, valid = _add_linear_phase(mask, wave, d_phase_x, d_phase_y)
        # Check for valid observation angle
        if valid == 0:
            _sum[n, m] = 0
            continue
        # Perform summation, take into account projected output area when calculating power
        _sum[n, m] = _perform_summation(u_1) * np.cos(x_ang) * np.cos(y_ang)
    # Return intensity taking into account projected input area
    return np.abs(_sum / np.sum(mask)) ** 2 * np.cos(inc[0]) * np.cos(inc[1])


def _tile_mask(n_x, n_y, mask):
    """ Takes in a mask pattern and tiles it in x and y"""
    y_dim, x_dim = mask.shape
    template = np.zeros((n_y * y_dim, n_x * x_dim))
    # Locate "outer" points
    x_locs = np.arange(0, n_x * x_dim, x_dim)
    y_locs = np.arange(0, n_y * y_dim, y_dim)
    for cur_x, cur_y in product(x_locs, y_locs):
        template[cur_y, cur_x] = 1
    # Locate "inner" points
    x_locs = np.arange(x_dim / 2, (n_x - 1) * x_dim, x_dim)
    y_locs = np.arange(y_dim / 2, (n_y - 1) * y_dim, y_dim)
    for cur_x, cur_y in product(x_locs, y_locs):
        template[cur_y, cur_x] = 1
    DMD_tiled = convolve2d(template, mask)
    return DMD_tiled[0:(n_y * y_dim), 0:(n_x * x_dim)]


def _perform_summation(u_1):
    """ Returns the E field magnitude in the far field for a given input
    wavefront u_1 """
    return np.sum(u_1)


def _add_linear_phase(mask, wave, d_phase_x, d_phase_y):
    """ Creates u_1 with appropriate mask and tilt phase
    mask : Single DMD mirror mask
    wave : wavelength, meters
    d_phase_x : delta x total [meters]
    d_phase_y : delta y total [meters]

    NOTES:
    -----
    -Does not take into account incident projected area when calculating
    phase amplitude
    -Assumes input phase is from a reflection-based system (opposite of
    transmission based). """

    # Check for under sampling
    x_spl = mask.shape[1]
    y_spl = mask.shape[0]
    # Check for aliasing with a factor of 2 sampling.
    if (x_spl < np.abs(d_phase_x) / wave * 3) or (y_spl < np.abs(d_phase_y) / wave * 3):
        warnings.warn('Insufficient sampling. Some of output image is clipped to avoid aliasing.')
        return 0, 0
    # NOTE: Does not take into accout the projected input/output area
    x_phase = np.linspace(d_phase_x / 2, -d_phase_x / 2, x_spl + 1)[0:x_spl]
    y_phase = np.linspace(d_phase_y / 2, -d_phase_y / 2, y_spl + 1)[0:y_spl]
    X_PHASE, Y_PHASE = np.meshgrid(x_phase, y_phase)
    PHASE = X_PHASE + Y_PHASE  # [m]
    complex_phase = np.exp(1j * 2 * np.pi / wave * PHASE)
    return mask * complex_phase, 1


def fresnel(u_1, x_width, y_width, wave, z_12):
    """
    INPUTS
    ------
    u_1 : array, float
        Magnitude of input E field
    x_width : float
        Width of input u_i mask, [meters]
    y_width : float
        Width of input y_i mask, [meters]
    wave : float
        Wavelength, [meters]
    z_12 : float
        Z distance from plane 1 to 2, [meters]
    OUTPUTS
    -------
    u_2 : float, array
        Array of same dimensions of input array of u_2 (E field at plane 2)
    r_max : float
        Maximum radius that Fresnel approximation is valid for.
    """
    # TODO: Add maximum range that this technique is valid given input, zero
    # padding, sapmpling, etc.
    Ny, Nx = u_1.shape
    dx = (x_width) / Nx
    dy = (y_width) / Ny
    # Check Fresnel conditions are met
    r_max = _check_fresnel_conditions(u_1, x_width, y_width, wave, z_12)
    # Calculate Fresnel diffraction transfer function
    H_12 = _H_12_fresnel(dx, dy, Nx, Ny, wave, z_12)
    # Perform wave propagation
    U_1 = fft2(fftshift(u_1))
    U_2 = U_1 * fftshift(H_12)
    # Mask data over valid radius
    u_2 = _mask_u_2(fftshift(ifft2(U_2)), r_max, x_width, y_width)
    return u_2


def _mask_u_2(u_2, r_max, x_width, y_width):
    """ Apply mask to output image defined by maximum radius that satisfies
    the 2 fresnel conditions.
    u_2 : array, output E field at output plane
    r_max : maximium valid radius defined by Fresnel conditions
    x_width : width of u_1, [meters]
    y_width : width of u_1, [meters]
    """
    Nx = u_2.shape[1]
    Ny = u_2.shape[0]
    xv = np.linspace(-x_width / 2, x_width / 2, Nx)
    yv = np.linspace(-y_width / 2, y_width / 2, Ny)
    X, Y = np.meshgrid(xv, yv)
    R = np.sqrt(X ** 2 + Y ** 2)
    return u_2 * (R < r_max).astype(np.float)


def _H_12_fresnel(dx, dy, Nx, Ny, wave, z_12):
    """ Computes the transfer function for the fresnel region
    dx : sample distance in x direction
    dy : sample distance in y direction
    Nx : number of samples in x direction
    Ny : number of samples in y direction
    wave : wavelength [meters]
    z_12 : propagation distance [meters]
    """
    xi = np.linspace(-1 / (2 * dx), 1 / (2 * dx), Nx)
    eta = np.linspace(-1 / (2 * dy), 1 / (2 * dy), Nx)
    XI, ETA = np.meshgrid(xi, eta)

    H_12_arg = 1j * 2 * np.pi / wave * z_12 - 1j * np.pi * wave * z_12 * (XI ** 2 + ETA ** 2)

    _H_12_check_aliasing(H_12_arg)  # Check for aliasing in transfer function

    return np.exp(H_12_arg)


def _H_12_check_aliasing(H_12_arg):
    """ Checks for aliasign in Fresnel transfer matrix
    """

    H_12_diff_x = np.zeros((H_12_arg.shape))
    H_12_diff_y = np.zeros((H_12_arg.shape))

    H_12_diff_y[0:-1, :] = np.diff(np.abs(H_12_arg), axis=0)
    H_12_diff_y[-1, :] = H_12_diff_y[0, :]

    H_12_diff_x[:, 0:-1] = np.diff(np.abs(H_12_arg), axis=1)
    H_12_diff_x[:, -1] = H_12_diff_x[:, 0]

    H_12_diff = np.sqrt(H_12_diff_x ** 2 + H_12_diff_y ** 2)

    print('Max derivative of H_12 {:.3f}'.format(np.max(H_12_diff)))
    print('Cuttoff: {:.3f}'.format(2. * np.pi / 8.))
    if np.max(H_12_diff > 2. * np.pi / 8.):
        print('Aliasing occuring in transfer matrix.')

#    plt.figure(figsize=(15, 5))
#    plt.plot(H_12_diff[0, :])
#    plt.title('Derivative of argument')
#    plt.grid()
#    plt.show()
#
#    plt.figure(figsize=(15, 5))
#    plt.plot(np.real(np.exp(H_12_arg[0, :])))
#    plt.title('exp of Argument')
#    plt.show()


def _check_fresnel_conditions(u_1, x_width, y_width, wave, z_12):
    """ Returns the maximum radial extent where Fresnel approximation is valid.
    Raises an error if both conditions are not met at all."""
    # Factor to determine "much greater than"
    fac = 10.
    # Check first Fresnel condition
    L_2_cond_1 = np.abs(z_12) / fac - _calculate_extent(u_1, x_width, y_width)
    if L_2_cond_1 <= 0:
        raise ValueError('Fresnel condition 1 failed. z_12 is too small.')
    # Check second Fresnel condition
    L_2_cond_2 = (4. * wave * np.abs(z_12) ** 3 / (fac * np.pi)) ** (0.25) - _calculate_extent(u_1, x_width, y_width)
    if L_2_cond_2 <= 0:
        raise ValueError('Fresnel condition 2 failed. Wavelength or z_12 are too small')
    return np.min([L_2_cond_1, L_2_cond_2])


def _calculate_extent(u_1, x_width, y_width):
    """ Calculates the maximum radial extent of the input u_1 field """
    # NOTE: The output is half the diagonal of the smallest rectangle that can
    # fit around data.
    # TODO: Make this the largest straight line that can fit in data

    # Assign distances to each pixel
    x = np.linspace(-x_width / 2, x_width / 2, u_1.shape[1])
    y = np.linspace(-y_width / 2, y_width / 2, u_1.shape[0])
    # Find nonzero elements in x direction
    x_nonzero = x[np.sum(u_1, axis=1) != 0]
    x_active_width = x_nonzero[-1] - x_nonzero[0]
    # Find nonzero elements in y direction
    y_nonzero = y[np.sum(u_1, axis=0) != 0]
    y_active_width = y_nonzero[-1] - y_nonzero[0]
    # Find diagonal defined by x and y maximum distances
    return np.sqrt(x_active_width ** 2 + y_active_width ** 2) / 2.


def rayleigh(u_1, x_range, y_range, wave, z_12):
    """ Calculates the Rayleigh-Sommerfield diffraction pattern of an input
    wave pattern. Takes the FFT of the input E wave pattern, u_1, and muliplies
    it with the Rayleigh-Sommerfield transfer function. The output u_2 is the
    IFFT of this product.
    INPUTS
    ------
    u_1 : array, float, shape should be centered and well zero padded.
        Shape of input E field
    x_range : list, float
        (x_min, x_max)
    y_range : list, float
        (y_min, y_max)
    wave : float
        Wavelength
    z_12 : float
        Z distance from plane 1 to 2
    OUTPUTS
    -------
    u_2 : float, array
        Array of same dimensions of input array of u_2 (E field at plane 2)
    """
    # TODO: Add maximum range that this technique is valid given input, zero
    # padding, sapmpling, etc.
    Ny, Nx = u_1.shape
    dx = (x_range[1] - x_range[0]) / Nx
    dy = (y_range[1] - y_range[0]) / Ny
    # Calculate Rayleigh tranfer function
    H_12 = _H_12_rayleigh(dx, dy, Nx, Ny, wave, z_12)
    # Perform wave propagation
    U_1 = fft2(fftshift(u_1))
    U_2 = U_1 * fftshift(H_12)
    return fftshift(ifft2(U_2))


def _H_12_rayleigh(dx, dy, Nx, Ny, wave, z_12):
    """ Creates the Rayleigh-Sommerfield transfer function """
    # TODO: Add description
    # TODO: Add maximum range that this technique is valid given input, zero
    # padding, sapmpling, etc.
    xi = np.linspace(-1 / (2 * dx), 1 / (2 * dx), Nx + 1)[0:Nx]
    eta = np.linspace(-1 / (2 * dy), 1 / (2 * dy), Nx + 1)[0:Ny]
    XI, ETA = np.meshgrid(xi, eta)
    expr = 1. - wave ** 2 * (XI ** 2 + ETA ** 2)
    return np.exp(1j * 2 * np.pi / wave * z_12 * np.sqrt(expr))
